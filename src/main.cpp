#include <iostream>
#include <cmath>
#include <cstdlib>
#include <cstdio>

#include "gl_core_43.h"

#define GLM_SWIZZLE
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>

using std::exit;

#define GLERRCHK { \
int err = glGetError(); \
if(err != GL_NO_ERROR) { \
	std::cerr << "Gl Error : " << err << " (" << __FILE__ << ':' << __LINE__ << ')' << std::endl; \
	exit(42); \
} \
}

#define PI  3.141592653589793
#define PI2 1.5707963267948966
#define PI3 1.0471975511965976
#define PI4 0.7853981633974483
#define PI5 0.6283185307179586
#define PI6 0.5235987755982989
#define PI7 0.4487989505128276
#define PI8 0.39269908169872414

#undef near
#undef far


// ----------------------------------------------------------------------------
// C++ site const/vars

const char *COMPUTE_SHADER =
	"#version 430 core\n"
	"layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;"

	// To translate light's coordinates to screen coordinates
	"uniform mat4 proj_m4;"
	"uniform mat4 view_m4;"

	// Camera position in space coordinates, for light culling
	"uniform vec3 camera_pos_3d;"

	"uniform vec2 screen_size;"
	"uniform vec2   tile_size;"
	"uniform int max_lights_per_tile;"

	// Depth texture made by the depth pre-pass
	"uniform sampler2D depth;"

	// Defines light structure
	"struct PointLight {"
		"vec4 position;"
		"vec4 color;"
		"float intensity;"
	"};"

	// Light SSBO (input)
	"layout(std430, binding = 0) buffer _pointLights {"
		"uint count;"
		"PointLight lights[];"
	"} point_lights;"

	// (output)
	"layout(std430, binding = 1) buffer _output {"
		"mat3x2 data[];"
	"} data_out;"

	// Computes the boundaries of the current tile, vec4 = (bl, ur)
	// bl = vec2(x, y) = bottom left  corner coordinates
	// ur = vec2(x, y) = upper  right corner coordinates
	"vec4 getTileBounds() {"
		"vec2 bl = gl_WorkGroupID.xy;" // WorkGroupID === TileID
		"bl = bl * tile_size;"
		"vec2 ur = bl + tile_size;"
		"if (ur.x > screen_size.x) ur.x = screen_size.x;"
		"if (ur.y > screen_size.y) ur.y = screen_size.y;"
		"return vec4(bl, ur);"
	"}"

	// Computes the depth frustum (Min and Max depth values) of the current tile
	// Returns vec2(min, max)
	"vec2 getDepthFrustum(vec4 boundaries) {"
		"float i, j;"
		"float min = 1., max = 0., d;"
		"for (i=boundaries.x; i<boundaries.z; i++) {"
			"for (j=boundaries.y; j<boundaries.w; j++) {"
				"d = texture(depth, vec2(i/screen_size.x, j/screen_size.y)).r;"
				"if (d > max) max = d;"
				"if (d < min) min = d;"
			"}"
		"}"
		"return vec2(min, max);"
	"}"

	// Returns the frustum of the current tile. mat3x2 = (xf, yf, zf)
	// xf = vec2(x1, x2)  yf = vec2(y1, y2)  zf = vec2(z1, z2)  .1 <= .2
	"mat3x2 getTileFrustum() {"
		"vec4 tile_bounds = getTileBounds();"
		"vec2 tile_depth  = getDepthFrustum(tile_bounds);"
		"tile_bounds.x = tile_bounds.x / (screen_size.x / 2.) -1;" // [0:win_w) to [-1:1] coordinates
		"tile_bounds.y = tile_bounds.y / (screen_size.y / 2.) -1;"
		"tile_bounds.z = tile_bounds.z / (screen_size.x / 2.) -1;"
		"tile_bounds.w = tile_bounds.w / (screen_size.y / 2.) -1;"
		"return mat3x2(tile_bounds.xz, tile_bounds.yw, tile_depth);"
	"}"

	// GLSL 430 has a distance() function, but there is no built-in squareDistance function
	"float sqrDistance(vec4 a, vec4 b) {"
		"vec4 ab = b - a;"
		"return ab.x * ab.x + ab.y * ab.y + ab.z * ab.z;"
	"}"
	"float sqrDistance(vec3 a, vec3 b) { return sqrDistance(vec4(a, 1.), vec4(b, 1.)); }"

	// Returns the frustum of a pointlight's area of effect. mat3x2 = (xf, yf, zf)
	// xf = vec2(x1, x2)  yf = vec2(y1, y2)  zf = vec2(z1, z2)  .1 <= .2
	"mat3x2 pointLightFrustum(int light_ind) {"
		"vec3 pos = point_lights.lights[light_ind].position.xyz;"
		"float intensity = point_lights.lights[light_ind].intensity;"
		"float att = intensity * 10. / sqrDistance(camera_pos_3d, pos);" // FIXME if ==0
		"float aspect_ratio = screen_size.x/float(screen_size.y);"
		"vec4 eye = view_m4 * vec4(pos, 1.);" // eye coordinates
		"vec4 scrn = proj_m4 * vec4(0., 0., eye.z+intensity, 1.);"
		"vec4 scrf = proj_m4 * vec4(0., 0., eye.z-intensity, 1.);"
		"float zn = (scrn.z / scrn.w) * .5 + .5;" // Z bounds of this light's frustum
		"float zf = (scrf.z / scrf.w) * .5 + .5;"
		"vec4 clip = proj_m4 * eye;" // clip coordinate
		"pos = clip.xyz / clip.w;" // screen coordinates (normalized)
		"return mat3x2(pos.x - att, pos.x + att, pos.y - (att * aspect_ratio), pos.y + (att * aspect_ratio), zn, zf);"
	"}"

	"void main() {"
		// Compute this tile's offset in output SSBO `indices`
		//"int tile_ind_offset = int(gl_WorkGroupID.y * floor(screen_size.x / tile_size.x) + gl_WorkGroupID.x);"

		// Frustum of the tile
		//"mat3x2 tile_frustum = getTileFrustum();"

		// Frustum of the pointlight
		//"mat3x2 pl_frustum = pointLightFrustum(int(gl_WorkGroupID.x));"
		//"data_out.data[int(gl_WorkGroupID.x)] = pl_frustum;"

		// Light Position
		"vec3 pos = point_lights.lights[int(gl_WorkGroupID.x)].position.xyz;"
		//"data_out.data[int(gl_WorkGroupID.x)] = mat3x2(vec2(pos.x), vec2(pos.y), vec2(pos.z));"

		// Distance cam -> light
		"data_out.data[int(gl_WorkGroupID.x)] = mat3x2(sqrDistance(camera_pos_3d, pos));"

	"}";

const glm::vec3 cam_pos(15., 8., 15.);
const glm::vec3 cam_orient(-PI8, PI4, 0.);
const glm::vec2 screen_size(800, 600);
const glm::vec2 tile_size(20, 20);
const glm::vec2 tile_count(40, 30);
const int tot_tile_count = 1200;
const int max_lights_per_tile = 4;
glm::mat4 proj_m4;
glm::mat4 view_m4;

ALLEGRO_FONT *font;
ALLEGRO_DISPLAY *display;

class PointLight {
public:
	glm::vec4 position;
	glm::vec4 color;
	GLfloat intensity;
	GLfloat realign[3];
};

// ----------------------------------------------------------------------------
// GLSL uniforms/buffers/... names

GLuint ssbo_in = 0, ssbo_out = 0;
GLuint cs_program = 0;
GLuint depth_tex = 0;
GLuint lc_proj_mat = 0, lc_view_mat = 0,
       lc_camera_pos_3d = 0,
       lc_screen_size = 0, lc_tile_size = 0, lc_max_lights_per_tile = 0,
       lc_depth = 0;


// ----------------------------------------------------------------------------

void init() {
	if (!al_init()) exit(1);

	al_set_app_name("DebugShader");
	al_set_new_display_flags(ALLEGRO_OPENGL);
	if (display = al_create_display(800, 600), !display) exit(2);

	// loads every OpenGL functions
	if (!ogl_LoadFunctions()) exit(3);

	// Clears the GL error buffer
	GLenum err;
	while (err = glGetError(), err != GL_NO_ERROR) {
		std::cerr << "Gl Error: " << err << std::endl;
	}

	if (!al_install_keyboard()) exit(4);
	if (!al_init_primitives_addon()) exit(5);
	al_init_font_addon();

	font = al_create_builtin_font();
}

// Compiles and links the compute Shader
void loadShader() {
	const GLchar **shaderSource = &COMPUTE_SHADER;

	// Compiles
	GLuint shader = glCreateShader(GL_COMPUTE_SHADER);
	glShaderSource(shader, 1, shaderSource, 0);
	glCompileShader(shader);

	int compiled = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if(compiled == GL_FALSE) {
		char *failStr = new char[250];
		glGetShaderInfoLog(shader, 250, NULL, failStr);
		glDeleteShader(shader);
		std::cerr << failStr << std::endl;
		exit(10);
	}

	// Attachs
	GLuint program = glCreateProgram();
	glAttachShader(program, shader);

	// Links
	int linked = 0;
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE) {
		char *failStr = new char[250];
		glGetProgramInfoLog(program, 250, NULL, failStr);
		glDeleteProgram(program);
		std::cerr << failStr << std::endl;
		exit(11);
	}

	glDetachShader(program, shader);
	glDeleteShader(shader);

	cs_program = program;
}

// Get GL names of uniforms in
void getUniforms() {
	GLuint ssbo[2];
	glGenBuffers(2, ssbo);
	ssbo_in  = ssbo[0];
	ssbo_out = ssbo[1];
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_out);
	glBufferData(GL_SHADER_STORAGE_BUFFER, 4 * 6*sizeof(GLfloat), NULL, GL_DYNAMIC_COPY);

	lc_proj_mat            = glGetUniformLocation(cs_program, "proj_m4");
	lc_view_mat            = glGetUniformLocation(cs_program, "view_m4");
	lc_camera_pos_3d       = glGetUniformLocation(cs_program, "camera_pos_3d");
	lc_screen_size         = glGetUniformLocation(cs_program, "screen_size");
	lc_tile_size           = glGetUniformLocation(cs_program, "tile_size");
	lc_max_lights_per_tile = glGetUniformLocation(cs_program, "max_lights_per_tile");
	lc_depth               = glGetUniformLocation(cs_program, "depth");GLERRCHK;
}

void loadDepth() {
	GLfloat *depth = (GLfloat*)malloc(screen_size.x * screen_size.y * sizeof(GLfloat));
	if (depth == NULL) exit(15);

	FILE *f = fopen("depth.float_array", "rb");
	if (f == NULL) exit(16);

	if (fread(depth, 1, 800*600*sizeof(float), f) != 800*600*sizeof(float)) exit(17);
	fclose(f);

	glGenTextures(1, &(depth_tex));
	glBindTexture(GL_TEXTURE_2D, depth_tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, screen_size.x, screen_size.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, depth);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);GLERRCHK;
}

void loadLights() {
	int size = 4*sizeof(PointLight) + 4*sizeof(GLuint);
	GLuint *adr = (GLuint*)std::malloc(size);
	if (adr == NULL) exit(20);
	*adr = 4;

	PointLight *ptr = (PointLight*)(adr + 4);
	glm::vec3 light_pos[4] = {
		glm::vec3(-11, .25, -11),
		glm::vec3(-11, .25,  11),
		glm::vec3( 11, .25, -11),
		glm::vec3( 11, .25,  11)
	};
	for (int i=0; i<4; i++) {
		ptr[i].color = glm::vec4(.5f);
		ptr[i].position = glm::vec4(light_pos[i], 0);
		ptr[i].intensity = 1.;
	}

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_in);
	glBufferData(GL_SHADER_STORAGE_BUFFER, size, adr, GL_STATIC_DRAW);GLERRCHK;
}

void retrieveData() {
	GLvoid *ptr;
	GLfloat *frusta;
	int id, tx, ty;
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_out);
	ptr = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
	frusta = (GLfloat*) ptr;

	glm::mat3x2 *res = (glm::mat3x2*) ptr;
	for (int i=0; i<4; i++) {
		std::printf("light_id=[%d]: (x1,x2) (y1,y2) (z1,z2) = (%f, %f) (%f, %f) (%f, %f)\n",
		            i,    res[i][0].x, res[i][0].y,    res[i][1].x, res[i][1].y,    res[i][2].x, res[i][2].y);
	}

	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
}

void clean() {
	al_destroy_font(font);
	al_uninstall_keyboard();
	al_destroy_display(display);
}

int main(void) {
	init();

	loadShader();
	glUseProgram(cs_program);
	getUniforms();

	loadDepth();
	glUniform3fv(lc_camera_pos_3d, 1, glm::value_ptr(cam_pos));
	glUniform2fv(lc_screen_size, 1, glm::value_ptr(screen_size));
	glUniform2fv(lc_tile_size, 1, glm::value_ptr(tile_size));
	glUniform1i(lc_max_lights_per_tile, max_lights_per_tile);

	// Actives texture unit#0, loads the depth texture in unit#0, and use unit#0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depth_tex);
	glUniform1i(lc_depth, 0);

	loadLights();

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, ssbo_in);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, ssbo_out);

	glDispatchCompute(4, 1, 1);

	retrieveData();GLERRCHK;

	clean();
	return 0;
}
